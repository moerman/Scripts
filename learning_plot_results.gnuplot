#!/usr/bin/env gnuplot

# used to plot learning performance.
# on the x axis: total test size
# on the y axis: learned number of states
# So the bigger the better

set terminal pngcairo size 1280,800
set output "results.png"
set key top left

# Log scale shows more details
set logscale x
# set xrange [0:1000000000]

# 3410 is the number of states of the SUL
set yrange [0:3410]

# lines with points (+)
style1 = "linespoints lw 2 pt 1 ps 2"
accum = "using 5:1 with @style1"

plot \
	"w_method_1.dat" @accum title "W-method",\
	"lee_yannakakis_1.dat" @accum title "L and Y",\
	"lee_yannakakis_randomized_more_2.dat" @accum title "L and Y + R",\
	"lee_yannakakis_randomized_more_3.dat" @accum title "L and Y + R",\
	"lee_yannakakis_randomized_more_heuristic_1.dat" @accum title "L and Y + R",\
	"lee_yannakakis_randomized_more_heuristic_2.dat" @accum title "L and Y + R",\
#	"lee_yannakakis_randomized_more_1.dat" @accum title "L and Y + R",\
#	"lee_yannakakis_randomized_1.dat" @accum title "L and Y + R",\
#	"lee_yannakakis_heuristic_randomized_1.dat" @accum title "L and Y + HR",\

