# Import the os module, for the os.walk function
import os
 
# make directories
def make_dirs(root, pathStructure):
    foo = os.path.split(pathStructure)
    if foo[0] == '':
        return
    make_dirs(root, foo[0])
    print('mkdir ', root, foo[0])    

# Set the directory you want to start from
rootDir = '.'
for dirName, subdirList, fileList in os.walk(rootDir):
    print('Found directory: %s' % dirName)
    make_dirs('bla', dirName)
    for fname in fileList:
        print('\t%s' % fname)
