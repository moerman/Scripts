
import os
import re
import sys

rootDir = 'models' # will traverse everything in here
outDir = 'out'     # will output results here for each model

whiteFilter = '.*' # will only do these (regex)
blackFilter = ''   # will skip these (regex)

learners = ['TTT']
testers = ['RandomisedHybridADS', 'RandomisedHSI', 'RandomisedWp', 'RandomisedW', 'RandomisedHybridUIOv']
iterations = 2

# Will add an extra option if it matches this
extras = [('.*bla.*', '-extraTraceESM -parallelism')]

def remove_prefix(text, prefix):
	if text.startswith(prefix):
		return text[len(prefix):]
	return text

def quoted(text):
	return '"' + text + '"'

def construct_dirs():
	dotMatch = '.*\.txt'
	here = os.getcwd()
	for dirName, subdirList, fileList in os.walk(rootDir):
		shortDirName = remove_prefix(remove_prefix(dirName, rootDir), "/")
		for fname in fileList:
			matchPath = os.path.join(shortDirName, fname)
			if not re.fullmatch(dotMatch, matchPath):
				continue
			if not re.fullmatch(whiteFilter, matchPath):
				continue
			if re.fullmatch(blackFilter, matchPath):
				continue

			extra = ""
			for (m, e) in extras:
				if re.fullmatch(m, matchPath):
					extra += " " + e

			basename, _ = os.path.splitext(fname)
			orgPath = os.path.join(here, dirName, fname)
			newPath = os.path.join(here, outDir, shortDirName, basename)
			os.makedirs(newPath, exist_ok=True)

			print ('doing it')
			yield (orgPath, newPath, extra)

all_files = [x for x in construct_dirs()]
[print (quoted(org), quoted(out), n+1, l, t, extra) for n in range(iterations) for l in learners for t in testers for (org, out, extra) in all_files]

print("If saved to a file, do: cat commands.txt | xargs -L 1 -P 30 ./myfunc.sh", file=sys.stderr)
