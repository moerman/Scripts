#!/bin/zsh

# Used to sanitize output from the learning experiment
# outputs 5 columns: #states, size of m queries, m acc queries, t queries, t acc queries
# Where size is total number of input symbols, m is for membership, t is for testing, acc is accumulative.

pbpaste \
	| sed '/ERROR.*$/d' \
	| sed '/^.*java.*$/d' \
	| sed 's/Hypo.*has //g;s/used //g;s/ states//g;s/ symb.*$//g;s/ que.*and / /g;s/.*CET.*$//g' \
	| paste -d ' ' - - - - - \
	| sed 's/^ *//g' \
	| awk '{macc+=$3} {tacc+=$5} {print $1, $3, macc, $5, tacc}'
