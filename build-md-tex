#!/bin/zsh

set -e

# This script converts a markdown file to latex and then to pdf. It does not
# do this in a smart way: it can only handle a single file source, but a
# preamble is allowed, it only compiles the source once, so references are
# broken. It is meant to give a fast impression of the markdown file.

inputfile="$1"
outputfile="${inputfile/.tex/.pdf}"

if [[ -z "$inputfile" ]]; then
	echo "usage: build-md-tex <filename.tex>"
	return 1
fi

echo "will use $inputfile to create $outputfile"

# we will do everything in a temporary directory (which is easily cleaned up)
temporary_directory=$(mktemp -d -t 'build-md-tex')
# the trap is run on any kind of exit (I believe)
trap 'rm -rf "$temporary_directory"' EXIT
echo "temporary files will be in $temporary_directory"


# determining the used filenames
copied_input="$temporary_directory/$inputfile"
preprocessed_input="$temporary_directory/${inputfile/.tex/.compiled.tex}"
output="$temporary_directory/${inputfile/.tex/.compiled.pdf}"


# first we copy all the needed resources
cp "$inputfile" "$copied_input"
cp "preamble.tex" "$temporary_directory/preamble.tex" || echo "no preamble found"
cp "references.bib" "$temporary_directory/references.bib" || echo "no references found"

# go to the temporary directory
cd "$temporary_directory"
# if there is a preamble, we will use it
[[ -f "preamble.tex" ]] && extra_flag1="-H preamble.tex"
# if there are references, we will use cite-proc
[[ -f "references.bib" ]] && extra_flag2="--filter pandoc-citeproc"
# pandoc: markdown (with latex) -> latex
pandoc -f markdown -t latex -s -N $=extra_flag1 $=extra_flag2 -o "$preprocessed_input" "$copied_input"
# pdflatex: latex -> pdf (and auxiliary files)
pdflatex -draftmode "$preprocessed_input"
pdflatex "$preprocessed_input"
# go back to previous dir
cd -


# and we're done (after copying back to original location)
cp "$output" "$outputfile"

