
# This "truncates" long labels in dot files
sed 's/label="........................................*"/label="truncated"/g' bla.dot > bla2.dot

# Strips some html tags I found in learnlib's output
sed 's/\[label.*<td>I/ \[label="/g;s/<\/td>.*<td>O/ \/ /g;s/<\/td>.*]/"\];/g' 

# Sufficient for distinguishing sequences and splitting trees
dot -O -Tpng -Goverlap=false *splitting_tree

# Might work on general machines (but not too big!)
neato -O -Tpng -Goverlap=false *.dot
