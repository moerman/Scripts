Scripts
=======

Here I store some scripts and commands I've used in different projects. Note:
some files just contain a bunch of commands I needed, so these are not supposed
to be run as a whole (just open them and copy/paste the relevant parts). Do not
expect this to be well documented, most scripts are ad hoc.
